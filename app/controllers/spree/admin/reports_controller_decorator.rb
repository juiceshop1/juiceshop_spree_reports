Spree::Admin::ReportsController.class_eval do
  
  ADDITIONAL= {
    sales_total: {name: 'Sales Total', description: 'Sales Total For All Orders' },
    daily_orders: {name: 'Daily Orders', description: 'CSV download of all orders for given date' }, 
    all_orders: {name: 'All Orders', description: "All Orders"}
  }

  Spree::Admin::ReportsController::AVAILABLE_REPORTS.merge!(ADDITIONAL)  
  
  def daily_orders
    respond_to do |format|
      format.html { render }
      format.csv { 
        delivery_date = params[:delivery_date] || Time.now.strftime("%Y/%m/%d")
        shipments = Spree::Shipment.where(scheduled_for: delivery_date)
        
        if shipments.size == 0
          flash[:notice] = "No Orers for #{delivery_date}"
          redirect_to daily_orders_admin_reports_url
          return
        end    
        
        file = Spree::Order.to_csv_for_download(delivery_date)

        # use Spree::Order class methods from JuiceShop spree site 
        send_data file, filename: "website_orders_#{delivery_date}.csv"
      }
    end
  end

  def all_orders
    respond_to do |format|
      format.html { render }
      format.csv { send_data Spree::Order.to_csv_for_download_all, filename: "website_orders_all.csv" }
    end
  end
end
