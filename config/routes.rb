Spree::Core::Engine.routes.draw do
  
  match '/admin/reports/daily_orders' => 'admin/reports#daily_orders',
    via: [:get, :post],
    as:  'daily_orders_admin_reports'

  match '/admin/reports/all_orders' => 'admin/reports#all_orders', 
    via: [:get, :post], 
    as: 'all_orders_admin_reports'
    
end
