# encoding: UTF-8
Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'juiceshop_spree_reports'
  s.version     = '1.3.3'
  s.summary     = 'Juiceshop Spree Reports'
  s.description = 'Juiceshop Spree Reports'
  s.required_ruby_version = '>= 2.0.0'

  s.author    = ['Weston Platter', 'Dustin Platte']
  s.email     = ['weston@think602.com', 'dustin@think602.com']
  s.homepage  = 'https://bitbucket.org/juiceshop1/juiceshop_spree_reports'

  #s.files       = `git ls-files`.split("\n")
  #s.test_files  = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.require_path = 'lib'
  s.requirements << 'none'

  s.add_dependency 'spree_core',  '~> 1.3.3'

  s.add_development_dependency 'capybara',           '~> 2.1'
  s.add_development_dependency 'coffee-rails'
  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'factory_girl',      '~> 4.2'
  s.add_development_dependency 'ffaker'
  s.add_development_dependency 'rspec-rails',       '~> 2.13'
  s.add_development_dependency 'sass-rails'
  s.add_development_dependency 'selenium-webdriver'
  s.add_development_dependency 'simplecov'
  s.add_development_dependency 'sqlite3'
end
